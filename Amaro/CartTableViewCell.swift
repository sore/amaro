//
//  CartTableViewCell.swift
//  Amaro
//
//  Created by Gustavo Luís Soré on 03/02/17.
//  Copyright © 2017 Sore. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var loadingImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var countLabel: UILabel!
    @IBOutlet var sizeLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var totalPriceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var viewController:CartViewController? = nil
    
    func setValues(item:CartItem){
        
        let image: UIImage? = item.image
        if image != nil {
            self.productImageView.image = item.image
            self.productImageView.contentMode = .scaleAspectFit
        }
        nameLabel.text = item.name
        countLabel.text = item.count
        sizeLabel.text = item.size
        priceLabel.text = item.price
        totalPriceLabel.text = "R$ \(String(format: "%.2f",Float(item.count)! * getFloatValue(price: item.price)))"
        totalPriceLabel.text = totalPriceLabel.text?.replacingOccurrences(of:".",with:",")
        
    }
    
    func getFloatValue(price: String) -> Float {
        
        let index = price.index(price.startIndex, offsetBy: 3)
        var s = price.substring(from: index) as String
        s = s.replacingOccurrences(of:",",with:".")
        return Float(s)!
        
    }

    @IBAction func deleteUnit(_ sender: Any) {
        let alert = UIAlertController(title: "Atenção", message: "Tem certeza que deseja deletar um item deste produto do carrinho?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Sim",
                                      style: UIAlertActionStyle.default,
                                      handler: {
                                        (alert: UIAlertAction!) in print("Esvaziar Carrinho")
                                        
                                        let item = DataManager.shared().cartArray[(self.viewController?.indexDetail)!]
                                        
                                        do{
                                            try CartItem.delete(itemToDelete: item, count: 1)
                                        } catch{
                                            print("Erro ao deletar um produto do carrinho.")
                                        }
                                        
                                        do{
                                            try CartItem.save(cartArray: DataManager.shared().cartArray)
                                        } catch{
                                            print("problema em salvar arquivo do carrinho")
                                        }
                                        
                                        self.viewController?.setTotalPriceLabelText()
                                        
                                        let range = NSMakeRange(0, (self.viewController?.tableView.numberOfSections)!)
                                        let sections = NSIndexSet(indexesIn: range)
                                        self.viewController?.tableView.reloadSections(sections as IndexSet, with: .fade)
                                        
                                        
        }))
        alert.addAction(UIAlertAction(title: "Não",
                                      style: UIAlertActionStyle.default,
                                      handler: {
                                        (alert: UIAlertAction!) in print("Não esvaziar Carrinho")
                                        
                                        
        }))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    @IBAction func deleteAll(_ sender: Any) {
        let alert = UIAlertController(title: "Atenção", message: "Tem certeza que deseja excluir todos os itens desse produto do carrinho?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Sim",
                                      style: UIAlertActionStyle.default,
                                      handler: {
                                        (alert: UIAlertAction!) in print("Esvaziar Carrinho")
                                        
                                        let item = DataManager.shared().cartArray[(self.viewController?.indexDetail)!]
                                        
                                        do{
                                            try CartItem.delete(itemToDelete: item, count: Int(item.count)!)
                                        } catch{
                                            print("Erro ao deletar um produto do carrinho.")
                                        }
                                        
                                        do{
                                            try CartItem.save(cartArray: DataManager.shared().cartArray)
                                        } catch{
                                            print("problema em salvar arquivo do carrinho")
                                        }
                                        
                                        self.viewController?.setTotalPriceLabelText()
                                        
                                        let range = NSMakeRange(0, (self.viewController?.tableView.numberOfSections)!)
                                        let sections = NSIndexSet(indexesIn: range)
                                        self.viewController?.tableView.reloadSections(sections as IndexSet, with: .fade)
                                        
                                        
        }))
        alert.addAction(UIAlertAction(title: "Não",
                                      style: UIAlertActionStyle.default,
                                      handler: {
                                        (alert: UIAlertAction!) in print("Não esvaziar Carrinho")
                                        
                                        
        }))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}
