//
//  SoreDateController.swift
//  SwiftTrainner
//
//  Created by Gustavo Luís Soré on 19/01/17.
//  Copyright © 2017 Sore. All rights reserved.
//

import Foundation

class DataManager {
    
    var products:Array<Product> = []
    var productsOnSale:Array<Product> = []
    var cartArray:Array<CartItem> = []
    
    var showOnlyProductsOnSale:Bool = false
    
    var didDownloadedImagesFromProducts = Bool.init(false)
    
    private static var sharedInstance: DataManager = {
        let instance = DataManager()
        
        return instance
    }()
    
    static func load() {
        
    }
    
    class func shared() -> DataManager {
        return sharedInstance
    }
    
    
}


