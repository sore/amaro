//
//  CatalogoTableViewCell.swift
//  Amaro
//
//  Created by Gustavo Luís Soré on 02/02/17.
//  Copyright © 2017 Sore. All rights reserved.
//

import UIKit

class CatalogoTableViewCell: UITableViewCell {

    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var loadingImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var colorLabel: UILabel!
    @IBOutlet var discountLabel: UILabel!
    
    @IBOutlet var installmentsLabel: UILabel!
    
    @IBOutlet var PPButton: UIButton!
    @IBOutlet var PButton: UIButton!
    @IBOutlet var MButton: UIButton!
    @IBOutlet var GButton: UIButton!
    @IBOutlet var GGButton: UIButton!
    
    var index:Int = -1
    var choosedSize:String = ""
    var productAux:Product? = nil
    
    func setCell(product:Product) {
        
        self.nameLabel.text = product.name
        if product.discount_percentage.isEmpty{
            self.priceLabel.text = product.regular_price
            self.discountLabel.isHidden = true
        } else{
            self.priceLabel.text = product.actual_price
            self.discountLabel.text = "- \(product.discount_percentage)"
            self.discountLabel.isHidden = false
        }
        self.colorLabel.text = "Cor: \(product.color)"
    }
    
    func buyCellComplement(product:Product) {
        
        clearButtons()
        
        self.installmentsLabel.text = product.installments
        
        if(product.sizes.count == 1){
            self.PPButton.isEnabled = false
            self.PPButton.setTitleColor(UIColor.lightGray, for: .normal)
            self.PButton.isEnabled = false
            self.PButton.setTitleColor(UIColor.lightGray, for: .normal)
            self.MButton.isEnabled = false
            self.MButton.setTitleColor(UIColor.lightGray, for: .normal)
            self.GButton.isEnabled = false
            self.GButton.setTitleColor(UIColor.lightGray, for: .normal)
            self.GGButton.isEnabled = false
            self.GGButton.setTitleColor(UIColor.lightGray, for: .normal)
            return;
        }
        
        self.configureteSizeButtons()
        
    }
    
    func configureteSizeButtons() {
        for size in (self.productAux?.sizes)! {
            
            if size.size == "PP" {
                if !size.available {
                    self.PPButton.isEnabled = false
                    self.PPButton.setTitleColor(UIColor.lightGray, for: .normal)
                }
            } else if size.size == "P" {
                if !size.available {
                    self.PButton.isEnabled = false
                    self.PButton.setTitleColor(UIColor.lightGray, for: .normal)
                }
            } else if size.size == "M" {
                if !size.available {
                    self.MButton.isEnabled = false
                    self.MButton.setTitleColor(UIColor.lightGray, for: .normal)
                }
            }else if size.size == "G" {
                if !size.available {
                    self.GButton.isEnabled = false
                    self.GButton.setTitleColor(UIColor.lightGray, for: .normal)
                }
            }else if size.size == "GG" {
                if !size.available {
                    self.GGButton.isEnabled = false
                    self.GGButton.setTitleColor(UIColor.lightGray, for: .normal)
                }
            }
            
        }
    }
    
    func clearButtons() {
        choosedSize = ""
        self.PPButton.setTitleColor(UIColor.black, for: .normal)
        self.PButton.setTitleColor(UIColor.black, for: .normal)
        self.MButton.setTitleColor(UIColor.black, for: .normal)
        self.GButton.setTitleColor(UIColor.black, for: .normal)
        self.GGButton.setTitleColor(UIColor.black, for: .normal)
        PPButton.backgroundColor = UIColor.white
        PButton.backgroundColor = UIColor.white
        MButton.backgroundColor = UIColor.white
        GButton.backgroundColor = UIColor.white
        GGButton.backgroundColor = UIColor.white
        configureteSizeButtons()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func PPAction(_ sender: Any) {
        if(choosedSize != ""){
            setNormalButtonSize(size: choosedSize)
        }
        PPButton.backgroundColor = UIColor.black
        PPButton.setTitleColor(UIColor.white, for: .normal)
        choosedSize = "PP"
    }
    @IBAction func PAction(_ sender: Any) {
        if(choosedSize != ""){
            setNormalButtonSize(size: choosedSize)
        }
        PButton.backgroundColor = UIColor.black
        PButton.setTitleColor(UIColor.white, for: .normal)
        choosedSize = "P"
    }
    @IBAction func MAction(_ sender: Any) {
        if(choosedSize != ""){
            setNormalButtonSize(size: choosedSize)
        }
        MButton.backgroundColor = UIColor.black
        MButton.setTitleColor(UIColor.white, for: .normal)
        choosedSize = "M"
    }
    @IBAction func GAction(_ sender: Any) {
        if(choosedSize != ""){
            setNormalButtonSize(size: choosedSize)
        }
        GButton.backgroundColor = UIColor.black
        GButton.setTitleColor(UIColor.white, for: .normal)
        choosedSize = "G"
    }
    @IBAction func GGAction(_ sender: Any) {
        if(choosedSize != ""){
            setNormalButtonSize(size: choosedSize)
        }
        GGButton.backgroundColor = UIColor.black
        GGButton.setTitleColor(UIColor.white, for: .normal)
        choosedSize = "GG"
    }
    
    func setNormalButtonSize(size: String) {
        
        if(size == "PP"){
            PPButton.backgroundColor = UIColor.white
            PPButton.setTitleColor(UIColor.black, for: .normal)
        } else if(size == "P"){
            PButton.backgroundColor = UIColor.white
            PButton.setTitleColor(UIColor.black, for: .normal)
        } else if(size == "M"){
            MButton.backgroundColor = UIColor.white
            MButton.setTitleColor(UIColor.black, for: .normal)
        } else if(size == "G"){
            GButton.backgroundColor = UIColor.white
            GButton.setTitleColor(UIColor.black, for: .normal)
        }else if(size == "GG"){
            GGButton.backgroundColor = UIColor.white
            GGButton.setTitleColor(UIColor.black, for: .normal)
        }
        
    }
    @IBAction func putProductInCart(_ sender: Any) {
        
        if choosedSize == "" && (productAux?.sizes.count)! > 1{
            
            let alert = UIAlertController(title: "Atenção", message: "Escolha um tamanho", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
            
        } else{
            Product.addProductToCart(product: productAux!, size: choosedSize)
            let alert = UIAlertController(title: "Sucesso", message: "Produto adicionado ao carrinho!", preferredStyle: UIAlertControllerStyle.actionSheet)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        }
        
    }

}
