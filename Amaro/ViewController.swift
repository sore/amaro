//
//  ViewController.swift
//  Amaro
//
//  Created by Gustavo Luís Soré on 31/01/17.
//  Copyright © 2017 Sore. All rights reserved.
//

import UIKit
import SystemConfiguration

class ViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{
    
    @IBOutlet var tableView: UITableView!
    
    var indexBuy:Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if DataManager.shared().products.isEmpty{
            do{
                DataManager.shared().products = try Product.loadFile(name: "products.json")
            } catch Product.ProductError.noFile{
                print("Ainda não possui carrinho de compra.")
            } catch Product.ProductError.cannotReadFile{
                print("Impossível ler o arquivo.")
            } catch Product.ProductError.jsonSerializationFailure{
                print("Erro ao parsear JSOn.")
            } catch Product.ProductError.jsonFormatError{
                print("Erro no formato do arquivo JSON.")
            } catch{
                
            }
            downloadProductsImages(products: DataManager.shared().products)
            if isInternetAvailable() == true{
                DataManager.shared().didDownloadedImagesFromProducts = true
            }
        }
        if DataManager.shared().didDownloadedImagesFromProducts == false{
            if isInternetAvailable() == true{
                downloadProductsImages(products: DataManager.shared().products)
                DataManager.shared().didDownloadedImagesFromProducts = true
            }
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.title? = "Catálogo"
        
        if DataManager.shared().showOnlyProductsOnSale {
            
            self.title? = "Produtos em promoção"
            
            if DataManager.shared().productsOnSale.isEmpty {
                
                for product in DataManager.shared().products {
                    
                    if product.on_sale {
                        
                        DataManager.shared().productsOnSale.append(product)
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if DataManager.shared().showOnlyProductsOnSale {
            return DataManager.shared().productsOnSale.count
        }
        return DataManager.shared().products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier:String
        
        if(indexBuy == indexPath.row){
            identifier = "catalogoCellBuy"
        } else{
            identifier = "catalogoCell"
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! CatalogoTableViewCell
        
        let product:Product
        
        if DataManager.shared().showOnlyProductsOnSale {
            product = DataManager.shared().productsOnSale[indexPath.row]
        } else{
            product = DataManager.shared().products[indexPath.row]
        }
        
        if product.didDownloadImage == false{
            var imgListArray = Array<UIImage>.init()
            for i in 1...8
            {
                
                let strImageName : String = "\(i).tiff"
                var image  = UIImage(named:strImageName)
                image = resizeImage(image: image!, targetSize: CGSize(width: 64, height: 64))
                imgListArray.append(image!)
            }
            
            cell.loadingImageView.animationImages = imgListArray;
            cell.loadingImageView.animationDuration = 1.0
            cell.loadingImageView.startAnimating()
        } else{
            cell.loadingImageView.isHidden = true
            
            let image = product.downloadedImage
            cell.backgroundImageView.image = image
            cell.backgroundImageView.contentMode = .scaleAspectFit
        }
        cell.productAux = product
        cell.setCell(product: product)
        if(indexBuy == indexPath.row){
            cell.buyCellComplement(product: product)
        }
        cell.selectionStyle = .none
        
        return cell
    }
    
    func downloadProductsImages(products: Array<Product>) {
        
        if isInternetAvailable() {
            DispatchQueue.global(qos: .userInitiated).async {
                
                for i in 0 ... DataManager.shared().products.count - 1{
                    
                    var product:Product
                    product = DataManager.shared().products[i]
                    
                    if product.image.isEmpty && (product.name.isEmpty == false){
                        product.downloadedImage = UIImage.init(named: "noImage.png")!
                        product.didDownloadImage = true
                        DataManager.shared().products[i] = product
                    } else{
                        let imageURL = URL.init(string: product.image)
                        if imageURL != nil{
                            let imageData:Data
                            do{
                                imageData = try Data.init(contentsOf: imageURL!)
                                product.downloadedImage = UIImage.init(data: imageData)!
                                product.didDownloadImage = true
                                DataManager.shared().products[i] = product
                            } catch{
                                product.downloadedImage = UIImage.init()
                                print("error, download image of product")
                            }
                        }
                    }
                    
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }
        } else {
            for i in 0 ... DataManager.shared().products.count - 1{
                
                var product:Product
                product = DataManager.shared().products[i]
                
                product.downloadedImage = UIImage.init(named: "noInternet.png")!
                DataManager.shared().products[i] = product
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexBuy != indexPath.row {
            indexBuy = indexPath.row
            let range = NSMakeRange(0, self.tableView.numberOfSections)
            let sections = NSIndexSet(indexesIn: range)
            tableView.reloadSections(sections as IndexSet, with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == indexBuy){
            return CGFloat(354)
        }
        return CGFloat(243)
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        // Source: http://stackoverflow.com/questions/31314412/how-to-resize-image-in-swift
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func isInternetAvailable() -> Bool
    {
        //Source: http://stackoverflow.com/questions/39558868/check-internet-connection-ios-10
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    
    
}

