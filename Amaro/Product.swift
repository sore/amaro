//
//  Product.swift
//  Amaro
//
//  Created by Gustavo Luís Soré on 01/02/17.
//  Copyright © 2017 Sore. All rights reserved.
//

import Foundation
import SystemConfiguration
import UIKit

struct Size {
    
    let available: Bool
    let size: String
    let sku: String
    
}

extension Size{
    
    static func load(dictionary:Dictionary<String,Any>) -> Size{
        
        let available = dictionary["available"] as! Bool
        let size = dictionary["size"] as! String
        let sku = dictionary["sku"] as! String
        
        return Size(
            available: available,
            size: size,
            sku: sku
        )
        
    }
    
}

struct Product {
    
    let name: String
    let style: String
    let code_color: String
    let color_slug: String
    let color: String
    let on_sale: Bool
    let regular_price: String
    let actual_price: String
    let discount_percentage: String
    let installments: String
    let image: String
    var downloadedImage: UIImage
    
    let sizes: Array<Size>
    
    var didDownloadImage: Bool
    
}

func pathToFile(name:String) -> String? {
    //let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
    let bundlePath = Bundle.main.bundlePath
    
    if name.isEmpty {
        return nil
    }
    
    return "\(bundlePath)/\(name)"
}

extension Product{
    
    enum ProductError: Error {
        
        case cannotReadFile
        case noFile
        case jsonSerializationFailure
        case jsonFormatError
        
    }
    
    static func loadFile( name:String ) throws -> Array<Product> {
        
        guard let file = pathToFile(name: name) else{
            
            throw ProductError.noFile
            
        }
        
        guard let data = NSData.init(contentsOfFile: file) else{
            
            throw ProductError.cannotReadFile
            
        }
        
        var rootDictionary:Dictionary<String,Any>
        
        do {
            rootDictionary = try JSONSerialization.jsonObject(with: data as Data, options: []) as! Dictionary<String,Any>
            
        } catch {
            throw ProductError.jsonSerializationFailure
        }
        
        let array:Array<Any> = rootDictionary["products"] as! Array
        
        var products = Array<Product>.init()
        
        for dict in array{
            
            let product:Product
            
            do{
                product = try Product.load(dictionary: dict as! Dictionary<String, Any>)
            } catch ProductError.jsonFormatError{
                throw ProductError.jsonFormatError
            }
            
            products.append(product)
            
        }
        
        return products
    }
    
    static func load (dictionary: Dictionary<String,Any>) throws -> Product {
        
        
        if let name = dictionary["name"] as? String,
        let style = dictionary["style"] as? String,
        let code_color = dictionary["code_color"] as? String,
        let color_slug = dictionary["color_slug"] as? String,
        let color = dictionary["color"] as? String,
        let on_sale = dictionary["on_sale"] as? Bool,
        let regular_price = dictionary["regular_price"] as? String,
        let actual_price = dictionary["actual_price"] as? String,
        let discount_percentage = dictionary["discount_percentage"] as? String,
        let installments = dictionary["installments"] as? String,
        let image = dictionary["image"] as? String{
            
            var sizes = Array<Size>.init()
            
            for dict in dictionary["sizes"] as! Array<Any> {
                
                let size = Size.load(dictionary: dict as! Dictionary<String,Any>)
                sizes.append(size)
                
            }
            
            return Product(
                name: name,
                style: style,
                code_color: code_color,
                color_slug: color_slug,
                color: color,
                on_sale: on_sale,
                regular_price: regular_price,
                actual_price: actual_price,
                discount_percentage: discount_percentage,
                installments: installments,
                image: image,
                downloadedImage: UIImage.init(),
                sizes: sizes,
                didDownloadImage: false
            )
            
        } else{
            
            throw ProductError.jsonFormatError
            
        }
        
        
    }
    
    static func addProductToCart(product:Product,size:String) {
        
        let price:String
        
        if product.on_sale == true {
            price = product.actual_price
        } else {
            price = product.regular_price
        }
        
        let item = CartItem(
            name: product.name,
            price: price,
            imageURLString: product.image,
            image: product.downloadedImage,
            size: size,
            count: "1")
        
        do{
           try CartItem.add(newItem: item)
        } catch{
           print("Erro ao adicionar produto no carrinho")
        }
        
    }
    
}
