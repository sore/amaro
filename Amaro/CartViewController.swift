//
//  CartViewController.swift
//  Amaro
//
//  Created by Gustavo Luís Soré on 03/02/17.
//  Copyright © 2017 Sore. All rights reserved.
//

import UIKit

class CartViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var totalPriceLabel: UILabel!
    
    var indexDetail:Int = -1

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableView.delegate = self
        tableView.dataSource = self
        
        setTotalPriceLabelText()
        setCartImages()
    }
    
    func setTotalPriceLabelText() {
        
        totalPriceLabel.text = "R$ \(String(cartTotal()))"
        
    }
    
    func setCartImages() {
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            if DataManager.shared().cartArray.count == 0 {
                return
            }
            
            for i in 0 ... DataManager.shared().cartArray.count-1 {
                
                var item = DataManager.shared().cartArray[i]
                
                let imageURLString = item.imageURLString
                
                for product in DataManager.shared().products {
                    
                    if imageURLString == product.image {
                        
                        if product.didDownloadImage{
                            
                            item.image = product.downloadedImage
                            DataManager.shared().cartArray[i] = item
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func getFloatValue(price: String) -> Float {
        
        let index = price.index(price.startIndex, offsetBy: 3)
        var s = price.substring(from: index) as String
        s = s.replacingOccurrences(of:",",with:".")
        return Float(s)!
        
    }
    
    func cartTotal() -> Float {
        
        if DataManager.shared().cartArray.count == 0 || DataManager.shared().cartArray.isEmpty{
            return 0
        }
        
        var item:CartItem
        
        var total:Float
        
        total = 0
        
        for i in 0 ... DataManager.shared().cartArray.count - 1{
            
            item = DataManager.shared().cartArray[i]
            let price = getFloatValue(price: item.price)
            total = total + (Float(item.count)! * price)
            
        }
        
        return total
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return DataManager.shared().cartArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var identifier = "cartCell"
        
        if(indexDetail == indexPath.row){
            identifier = "detailCartCell"
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! CartTableViewCell
        
        let item = DataManager.shared().cartArray[indexPath.row]
        
        cell.setValues(item: item)
        
        cell.viewController = self
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexDetail != indexPath.row {
            indexDetail = indexPath.row
            let range = NSMakeRange(0, self.tableView.numberOfSections)
            let sections = NSIndexSet(indexesIn: range)
            tableView.reloadSections(sections as IndexSet, with: .fade)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == indexDetail){
            return CGFloat(290)
        }
        return CGFloat(210)
    }
    
    @IBAction func placeOrder(_ sender: Any) {
        
        let alert = UIAlertController(title: "Atenção", message: "Funcionalidade ainda não desenvolvida.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }

    @IBAction func emptyCart(_ sender: Any) {
        
        let alert = UIAlertController(title: "Atenção", message: "Tem certeza que deseja esvaziar o carrinho?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Sim",
                                      style: UIAlertActionStyle.default,
                                      handler: {
                                        (alert: UIAlertAction!) in print("Esvaziar Carrinho")
                                        
                                        DataManager.shared().cartArray = []
                                        do{
                                            try CartItem.save(cartArray: DataManager.shared().cartArray)
                                        } catch{
                                            print("problema em salvar arquivo do carrinho")
                                        }
                                        
                                        
                                        let range = NSMakeRange(0, self.tableView.numberOfSections)
                                        let sections = NSIndexSet(indexesIn: range)
                                        self.tableView.reloadSections(sections as IndexSet, with: .fade)
                                        self.setTotalPriceLabelText()
                                        
                                        
        }))
        alert.addAction(UIAlertAction(title: "Não",
                                      style: UIAlertActionStyle.default,
                                      handler: {
                                        (alert: UIAlertAction!) in print("Não esvaziar Carrinho")
                                        
                                        
        }))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        
        
    }

}
