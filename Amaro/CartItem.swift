//
//  CartItem.swift
//  Amaro
//
//  Created by Gustavo Luís Soré on 03/02/17.
//  Copyright © 2017 Sore. All rights reserved.
//

import Foundation
import UIKit

struct CartItem{
    
    let name:String
    let price:String
    let imageURLString:String
    var image:UIImage
    let size:String
    var count:String
    
}

func filePath(name:String) -> String? {
    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
    
    if name.isEmpty {
        return nil
    }
    
    return "\(documentsPath)/\(name)"
}

extension CartItem{
    
    enum CartItemError: Error {
        
        case cannotReadFile
        case noFile
        case jsonSerializationFailure
        case removeOldCartJSONFileFailure
        case jsonFormatError
        
    }
    
    static func loadFile( name:String ) throws -> Array<CartItem> {
        
        guard let file = filePath(name: name) else{
            
            throw CartItemError.noFile
            
        }
        
        guard let data = NSData.init(contentsOfFile: file) else{
            
            throw CartItemError.cannotReadFile
            
        }
        
        let array:Array<Any>
        
        do {
            array = try JSONSerialization.jsonObject(with: data as Data, options: []) as! Array<Any>
            
        } catch {
            throw CartItemError.jsonSerializationFailure
        }
        
        var itensArray = Array<CartItem>.init()
        
        for dict in array{
            
            let itens:CartItem
            
            do{
                itens = try CartItem.load(dictionary: dict as! Dictionary<String, Any>)
            } catch CartItemError.jsonFormatError{
                throw CartItemError.jsonFormatError
            }
            
            itensArray.append(itens)
            
        }
        
        return itensArray
    }
    
    static func load (dictionary: Dictionary<String,Any>) throws -> CartItem {
        
        
        if let name = dictionary["name"] as? String,
            let price = dictionary["price"] as? String,
            let imageURLString = dictionary["imageURLString"] as? String,
            let size = dictionary["size"] as? String,
            let count = dictionary["count"] as? String{
            
            return CartItem(
                name: name,
                price: price,
                imageURLString: imageURLString,
                image: UIImage.init(),
                size: size,
                count: count
            )
            
        } else{
            
            throw CartItemError.jsonFormatError
            
        }
        
        
    }
    
    static func dictionary(item:CartItem) -> Dictionary<String,Any> {
        
        var dict = Dictionary<String,Any>.init()
        
        dict["name"] = item.name
        dict["price"] = item.price
        dict["imageURLString"] = item.imageURLString
        dict["size"] = item.size
        dict["count"] = item.count
        
        return dict
        
    }
    
    static func save (cartArray:Array<CartItem>) throws{
        
        guard let filePath = filePath(name: "cart.json") else {
            
            throw CartItemError.noFile
            
        }
        
        var array = Array<Dictionary<String,Any>>.init()
        
        for item in cartArray {
            
            array.append(dictionary(item: item))
            
        }
        
        var data:Data
        
        do{
            data = try JSONSerialization.data(withJSONObject: array, options: [.prettyPrinted])
        }
        catch{
            
            throw CartItemError.cannotReadFile
            
        }
        
//        guard let file = FileHandle(forWritingAtPath:filePath) else {
//            
//            throw CartItemError.jsonSerializationFailure
//            
//        }
        
        let manager = FileManager.default
        if manager.fileExists(atPath: filePath) == true{
            
            do{
                try manager.removeItem(atPath: filePath)
            } catch{
                throw CartItemError.removeOldCartJSONFileFailure
            }
            
        }
        
        manager.createFile(atPath: filePath, contents: data, attributes: nil)
        
    }
    
    static func add(newItem: CartItem) throws {
        
        var item:CartItem
        
        if DataManager.shared().cartArray.count == 0 {
            DataManager.shared().cartArray.append(newItem)
            
            do{
                try self.save(cartArray: DataManager.shared().cartArray)
            } catch{
                print("Erro ao salvar carrinho")
            }
            return
        }
        
        for i in 0 ... DataManager.shared().cartArray.count - 1{
            
            item = DataManager.shared().cartArray[i]
            
            if item.name == newItem.name {
                
                if item.size == newItem.size {
                    
                    item.count = String(Int(item.count)! + 1)
                    DataManager.shared().cartArray[i] = item
                    do{
                        try self.save(cartArray: DataManager.shared().cartArray)
                    } catch{
                        print("Erro ao salvar carrinho")
                    }
                    return;
                    
                }
                
            }
            
        }
        
        DataManager.shared().cartArray.append(newItem)
        
        do{
            try self.save(cartArray: DataManager.shared().cartArray)
        } catch{
            print("Erro ao salvar carrinho")
        }
        
    }
    
    static func delete(itemToDelete: CartItem,count: Int) throws {
        
        var item:CartItem
        
        for i in 0 ... DataManager.shared().cartArray.count{
            
            item = DataManager.shared().cartArray[i]
            
            if item.name == itemToDelete.name {
                
                if item.size == itemToDelete.size {
                    
                    item.count = String(Int(item.count)! - count)
                    if Int(item.count)! <= 0{
                        DataManager.shared().cartArray.remove(at: i)
                    } else{
                        DataManager.shared().cartArray[i] = item
                    }
                    do{
                        try self.save(cartArray: DataManager.shared().cartArray)
                    } catch{
                        print("Erro ao salvar carrinho")
                    }
                    return;
                    
                }
                
            }
            
        }
        
    }

    
}
